# This is the Kinect module of Bugtiply
# Carlos Lugo Velez, Eashan Saikia and Francois Nedelec,
# Copyright Sainsbury Laboratory at Cambridge University, UK
# Version July 4 2022
import sys, os, math
import cv2 as cv

from pylibfreenect2 import Freenect2, SyncMultiFrameListener
from pylibfreenect2 import FrameType, Registration, Frame
from pylibfreenect2 import OpenGLPacketPipeline, CpuPacketPipeline
#from pylibfreenect2 import createConsoleLogger, setGlobalLogger
#from pylibfreenect2 import LoggerLevel

#orientation of the hand: 1 = vertical
HAND = 0
#number of pixels to be considered
area = 256
# depth used to calculate weights (should be < 500)
ground = 500
# number of pixels discarded at border of image:
g1 = 64
g2 = 64

def computeMoments(img, H, W):
    c = 0
    s = 0
    sy = 0
    sx = 0
    sxx = 0
    syy = 0
    for i in range(H):
        for k in range(W):
            w = img[i,k]
            if w > 0.0:
                c += 1
                s += w
                sx += w*i
                sy += w*k
                sxx += w*i*i
                syy += w*k*k
    if c > 0:
        sx = sx/s
        sy = sy/s
        sxx = sxx/s-sx*sx
        syy = syy/s-sy*sy
        s = s/c
    return ( s, sx, sy, sxx, syy )


def computeCutoff(img, base, arg):
    hist = cv.calcHist([img],[0],None,[1500-base],[base,1500])
    res = base
    nnz = cv.numpy.nonzero(hist)[0]
    if len(nnz):
        inf = nnz[0] + base
        sum = 0
        for i in nnz:
            #print(i,k,sum)
            sum += hist[i]
            if sum >= arg:
                return base + i, sum, inf
    return base, 0, base


def crop(arg, c1, c2):
    H, W = arg.shape
    return arg[c1:H-c1, c2:W-c2]


def process(depth, H, W):
    global area, ground;
    global HAND;
    res = ()
    cutoff, sum, inf = computeCutoff(depth, 500, area)
    mask = ( depth >= cutoff )
    #print(' min %i cutoff %i (area %i) '%(inf, cutoff, sum), end=' ')
    if 64 < sum and sum < 2*area:
        # weigh pixels that are closer to the camera
        base = ( ground + inf ) / 2
        weight = ( cutoff - base ) / ( depth - base )
        weight[mask] = 0.0
        Z,sx,sy,sxx,syy = computeMoments(weight, H, W)
        if Z > 0:
            Z = ground + (cutoff-ground) / Z
        if sxx + syy > 0:
            R = (sxx-syy)/(sxx+syy)
        else:
            R = 0
        #print("%7.2f %7.2f %7.2f %7.2f : %7.2f"%(sx,sy,sxx,syy,Z))

        #print(": %7.2f %7.2f %7.2f : %+.2f %i "%(sy,sx,Z,R,HAND), end=' ')
        ###############
        if 1:
            #print("weight %.2f %.2f "%(weight.min(axis=(0, 1)), weight.max(axis=(0, 1))), end=' ')
            # display 'weigths' as an image:
            scale = ( inf - base ) / ( cutoff - base )
            #scale = 1.0 / weight.max(axis=(0, 1))
            img = cv.cvtColor(scale*weight, cv.COLOR_GRAY2RGB)
            c0, c1 = float(sy), float(sx)
            d0, d1 = math.sqrt(syy), math.sqrt(sxx)
            img = cv.rectangle(img, (int(c0-d0), int(c1-d1)), (int(c0+d0), int(c1+d1)), (0,255,0), 1)
            img = cv.line(img, (int(c0), int(c1-d1)), (int(c0), int(c1+d1)), (0,0,255), 1)
            img = cv.line(img, (int(c0-d0), int(c1)), (int(c0+d0), int(c1)), (0,0,255), 1)
            cv.imshow('Scale', img)
        ACT = HAND
        margin = 0.20
        if HAND:
            if -R > margin:
                ACT = 4
                HAND = 0
        else:
            if R > margin:
                ACT = 5
                HAND = 1
        res = (sx, sy, ACT)
    return res

######################### KINECT ######################

fn = Freenect2()
num_devices = fn.enumerateDevices()
if num_devices == 0:
    print("No Kinect connected!")
    sys.exit(1)

try:
    #pipeline = OpenGLPacketPipeline()
    pipeline = OpenGLPacketPipeline()
    print("Kinect using OpenGL pipeline")
except:
    pipeline = CpuPacketPipeline()
    print("Kinect using CPU pipeline")

serial = fn.getDeviceSerialNumber(0)
device = fn.openDevice(serial, pipeline=pipeline)
#listener = SyncMultiFrameListener(FrameType.Color|FrameType.Ir|FrameType.Depth)
listener = SyncMultiFrameListener(FrameType.Depth)
#device.setColorFrameListener(listener)
device.setIrAndDepthFrameListener(listener)
device.start()

###########################  OPENCV  ###############################
zoom = 4
kernel = cv.getStructuringElement(cv.MORPH_RECT, (5,5))

def change_g2(arg): global g2; g2 = arg

def change_g1(arg): global g1; g1 = arg

def change_area(arg): global area; area = arg

cv.namedWindow('Depth', cv.WINDOW_NORMAL)
if 0:
    cv.createTrackbar('area', 'Depth', area, 2000, change_area)
    cv.setTrackbarMin('area', 'Depth', 100)
    cv.createTrackbar('g1', 'Depth', g1, 130, change_g1)
    cv.createTrackbar('g2', 'Depth', g2, 130, change_g2)

cv.namedWindow('Scale', cv.WINDOW_NORMAL)
cv.moveWindow('Scale', 0, zoom*384//2)

try:
    # that's necessary on MacOS for some reason...
    os.chdir('/Users/nedelec/code/bugtiply')
except FileNotFoundError:
    print("that's okay")

try:
    pipe = os.open('fifo', os.O_WRONLY)
    print("Pipe open @ %i"%pipe)
except PermissionError:
    pipe = 0
except FileNotFoundError:
    pipe = os.mkfifo('fifo')
    pipe = os.open('fifo', os.O_WRONLY)


while True:
    frames = listener.waitForNewFrame()
    depth = crop(frames["depth"].asarray(), g1, g2).copy()
    listener.release(frames)
    # push edge pixels to infinity:
    depth[depth<ground] = 4500
    if 0:
        print("image size %i %i "%depth.shape, end=' ')
        print("range %i %i "%(depth.min(axis=(0, 1)), depth.max(axis=(0, 1))), end=' ')
    if 0:
        # this would average two frames:
        frames = listener.waitForNewFrame()
        depth2 = crop(frames["depth"].asarray(), g1, g2)
        depth2[depth<ground] = 4500
        depth = ( depth + depth2 ) / 2
        listener.release(frames)
    if 1:
        # apply the opening morphological filter:
        depth = cv.morphologyEx(depth, cv.MORPH_OPEN, kernel)
        # downsample:
        depth = cv.resize(depth, (depth.shape[1]//2, depth.shape[0]//2))
        depth = cv.GaussianBlur(depth, (5,5), 0)
        b1 = int(g1/2)
        b2 = int(g2/2)
    else:
        b1 = int(g1)
        b2 = int(g2)
    H, W = depth.shape
    cv.resizeWindow('Scale', zoom*W, zoom*H)
    if 1:
        # clear pixels on the borders:
        c1 = int(b1/2)
        c2 = int(b2/2)
        img = cv.numpy.full(depth.shape, 4500, depth.dtype)
        img[c2:H-c2, c1:W-c1] = depth[c2:H-c2, c1:W-c1]
        depth = img
    if 1:
        # display image in pseudo colors with cropping rectangle
        cv.resizeWindow('Depth', zoom*W, zoom*H)
        img = 255-(depth-500)*(255/4000)
        img = img.astype(cv.numpy.uint8)
        img = cv.applyColorMap(img, cv.COLORMAP_JET)
        # draw recangle where data is considered:
        img = cv.rectangle(img, (b2,b1), (W-b2, H-b1), (255,255,255), 1)
        cv.imshow('Depth', img)
    a = process(depth, H, W)
    if a:
        #print("size %i %i "%(W,H), end=' ')
        #print("cursor %4.1f %4.1f "%(a[1], a[0]), end=' ')
        X = (a[1]-b2)/(W-2*b2)
        Y = (a[0]-b1)/(H-2*b1)
        #X = max(0, min(1, X))
        #Y = max(0, min(1, Y))
        B=b"%.5f %.5f %d\n"%(X, Y, a[2])
        if pipe > 0:
            os.write(pipe, B)
            print('>>>>' + B.decode(), end='')
        else:
            print('<<<<' + B.decode(), end='')
    if cv.waitKey(8)==27:
        break

device.stop()
device.close()
cv.destroyAllWindows()
