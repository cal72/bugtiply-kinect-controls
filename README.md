# BUGTIPLY KINECT INTERACTIVE PROGRAM

This code is the base for interacting with the evolutionary game **bugtiply** and can be used as a template for further interactive installations.

## Dependencies.

The script makes use of the [freenect2](https://github.com/OpenKinect/libfreenect2) kinect drivers and [OpenCV](https://opencv.org/).

## Description.

The script is meant to implement four actions which are used in the game to copy, kill, restart and move the cursor in the game. The script removes the background, isolates the hand connected component and calculates the centroid, area, convex hull and contour to obtain a bounding box, the hand circularity, convexity and box aspect ratio  which can be used to implement a set of commands. The normalised  centroid coordinates and the command are then witten to a `fifo` pipe which is read by the game.

## Files

* bugtiply.py : latest version
* bugtiply_cv.py : version used in June 2022

The two versions differ in the way the Kinect image is displayed onscreen: using OpenCV or MatPlotLib.

